# build an ISO image that will auto install NixOS and reboot
# $ nix-build make-iso.nix
with import <nixpkgs/lib>;

let
   config = (import <nixpkgs/nixos/lib/eval-config.nix> {
     system = "x86_64-linux";
     modules = [
       <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>
       ({ pkgs, lib, ... }:
       {
           console = {
             keyMap = "de";
           };

           services.openssh.permitRootLogin = "yes";
           environment.systemPackages = with pkgs; [
             (
               pkgs.writeTextFile {
                 name = "themelios";
                 destination = "/bin/themelios";
                 executable = true;
                 text = (builtins.readFile ./themelios);
               }
             )
           ];
           users.extraUsers.root.initialPassword = lib.mkForce "26sadfSADFDtr";
           services.openssh.enable = true;

           systemd.services.themelios = {
             description = "Self-bootstrap a NixOS installation";
             wantedBy = [ "multi-user.target" ];
             after = [ "network.target" "polkit.service" ];
             path = [ "/run/current-system/sw/" ];
             script = with pkgs; ''
               #! ${pkgs.bash}/bin/bash

               yes | themelios ./hosts/default/configuration.sh https://gitlab.com/Preisschild/themelios.git master
             '';
             environment = config.nix.envVars // {
               HOME = "/root";
             };
             serviceConfig = {
               Type = "oneshot";
             };
          };

       })
     ];
   }).config;
in
  config.system.build.isoImage
