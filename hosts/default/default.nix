{ config, pkgs, lib, ... }:
# just an example top-level "configuration.nix" file within the themelios scheme
{
imports = [];

console = {
  keyMap = "de";
};

time.timeZone = "Europe/Vienna";

programs.bash.enableCompletion = true;

networking.hostName = "nixos-preprovisioned";

users.extraUsers.root = {
  initialPassword = lib.mkForce "26sadfSADFDtr";
  openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDYB452Ie4Iyu8ZJvYlukiPS+5pUe+H/wT78qpdvsOM2ir6brjf7uZktDFl1QLrMRGi7CwccIWm16F0dGfV1H5SaleTUeHJhnqMRAGx9OFs/UwiloW5rKQ9BPSxXWhI6GL2qrKiJyc0S1bc2z2v7NgzyueCBrto6wQ7CmQy/Mwt/deSLmsK7Frglh+UZGdIpq4VHXVrtHDHtqNur7vpsrhPXzb9UGK2G+RIq8OmxGevbJ5vgtiXHQqg2mBOYGol97hRNUOm6p2zKCSCTq7Qg7DTpCGpwkBJbudTpvoieIvlPHW6rfcLLgBoDfdHk56+y8N5hGgyHthuXHmihtONXJvLZ+TaPndO87K/8WRabVNj9Korw916bwtvZrNcqI2EJ4fwVMR4O5dZdLy2eFtaPkBG6yiPIrZ1Y6n5HaSU6AGzfg47tRWd1KZePEh6PP6powSP8wfZ3VhQm49mMwowujb18cTrqi6aneO7touYqrx5vZ6GVJYRSjggJL6sierNFaTxILK0q5byuaNgl8/LFnjGhbd4gIeUhcQM/t8AD/16KoXY2hVlvvgWagVdCisP1qlilVhz6HaaepsChGkhK1aTqPb9kPf9k8gi37iE1txX7wLw+a2VZPUmW96YpxNjkMAaTyVg67YtvZRpvxRY0NLuPh7AMVC79ZaXnzvIYc34Ow== preisschild@PreisschildPC" ];
};

services.openssh = {
  enable = true;
  permitRootLogin = "yes";
};

}
